#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
AkaNamen App
"""

from kivy.app import App
from app.ext import MenuScreen, GameScreen, AboutScreen
from app.ext.callbacks import exit
from app.constants import SM, SM_GAME, SM_MENU, SM_ABOUT

import kivy
kivy.require('1.10.1')


class AkanamenApp(App):
    def build(self):
        SM.add_widget(MenuScreen(name=SM_MENU))
        SM.add_widget(GameScreen(name=SM_GAME))
        SM.add_widget(AboutScreen(name=SM_ABOUT))
        return SM

    def on_start(self):
        from kivy.base import EventLoop
        EventLoop.window.bind(on_keyboard=self.hook_keyboard)

    def hook_keyboard(self, window, key, *largs):
        if key == 27:
            if SM.current == SM_MENU:
                exit(self)
            else:
                SM.current = SM_MENU

            return True


AkanamenApp().run()
