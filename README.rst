========
AkaNamen
========

A simple name learning app.
Tailore specifically for the needs of the AkaBlas_, but simple to customize.
Build on Kivy_.

.. _AkaBlas: https://akablas.de
.. _Kivy: https://kivy.org

Developing
==========

Get Kivy_ and Buildozer_ to work. It's a hassle. Don't get me started …

.. _Kivy: https://kivy.org
.. _Buildozer: https://buildozer.readthedocs.io/en/latest/

Clone and install dependencies:

.. code:: shell

    git clone https://gitlab.com/akanamen/app.git --recurive
    cd app
    sudo pip install -r requirements.txt -r requirements-dev.txt

Install pre-commit hooks:

.. code:: shell

    pre-commit install

To compile the app for Android:

.. code:: shell

    buildozer android debug

Install on connected device:

.. code:: shell

    adb install -r "/path/to/akanamen/bin/AkaNamen-0.1-debug.apk"

Read Log while runnig on connected device:

.. code:: shell

    adb logcat -s python

Format code with YAPF:

.. code:: shell

    yapf -vv -ir . --style pep8 --exclude .buildozer/
    
Roadmap
=======

    * Make Buttons and Labels responsive to display size

    