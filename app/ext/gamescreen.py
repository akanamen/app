#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the GameScreen class."""

from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.event import EventDispatcher
from kivy.properties import StringProperty
from kivy.uix.popup import Popup
from .akablase import get_akablasen
from .callbacks import set_current_screen
from .onecolumnscreen import OneColumnScreen
from .akaimagebutton import AkaImageButton
from app.util import AkaButton, AkaLabel, AkaBlas_Yellow
from app.constants import REL_DATABASE_PATH as DATABASE
from app.constants import SM_MENU, WIDTH, VALIGN
from random import shuffle
import os


class GameScreen(OneColumnScreen, EventDispatcher):
    """
    Custom Screen. Use to display the game.

    Attributes:
        show (:obj:`str`): The attritubet to show as a hint.
        ask (:obj:`str`): The attribute to ask for.
        show_widget (:class:`Widget`): The widget displaying the known information
        ask_widget (:class:`Widget`): The widget displaying a set of possible choices for
            the answer to be selected from
        akablasen (List[:class:`app.AkaBlase`]): A list of all AkaBlasen in the database.
        current_shown (:class:`app.AkaBlase`): The AkaBlase currently on display (i.e.
            :attr:`show_widget`) is equipped with data from this AkaBlase.
        current_ask (List[:class:`app.AkaBlase]): The AkaBlasen currently providing
            information for :attr:`ask_widget`.
    """

    ask = StringProperty(allownone=True)
    show = StringProperty(allownone=True)

    def __init__(self, **kwargs):
        super(GameScreen, self).__init__(**kwargs)

        self.show_widget = None
        self.ask_widget = None
        self.akablasen = get_akablasen()
        shuffle(self.akablasen)

        self.current_show = None
        self.current_ask = []

        self.menu_button = AkaButton(
            text='Menü',
            size_hint=(0.2, 0.1),
            next_screen=SM_MENU,
            on_press=set_current_screen)
        self.show_dummy = AkaLabel(text='Bitte eine Frage wählen.')
        self.ask_dummy = AkaLabel(text='Bitte zuerst eine Frage wählen.')

        self.popup = None

        self.build_screen()

    def next(self, obj=None, update=True):
        """
        Updates screen to show next question.

        Args:
            obj (:class:`Button`): Optional. If used as callback, the widget originating
                the callback.
            update (:obj:`bool`): Optional. Wether :attr:`current_show` and
                :attr:`current_ask` shall be updated.
        """
        if update:
            if self.show:
                self.next_current_show()

                if self.current_show:
                    if self.show == 'image':
                        self.show_widget = Image(
                            source=os.path.join(DATABASE, self.current_show.
                                                image))
                        self.show_widget.size_hint = (WIDTH, 0.5)
                    else:
                        self.show_widget = AkaLabel(
                            text=self.current_show.__getattribute__(self.show))
                        self.show_widget.size_hint = (WIDTH, 0.3)
                    self.show_widget.font_size = 30

            if self.ask:
                self.next_current_ask()

                if self.current_ask:
                    self.ask_widget = GridLayout(
                        cols=2, size_hint_x=WIDTH
                        # size_hint=(0.2, 0.2),
                    )
                    for iter in range(4):
                        if self.ask == 'image':
                            self.ask_widget.add_widget(
                                AkaImageButton(
                                    source=os.path.join(
                                        DATABASE,
                                        self.current_ask[iter].image),
                                    akablase=self.current_ask[iter],
                                    on_press=self.handle_answer,
                                ))
                        else:
                            self.ask_widget.add_widget(
                                AkaButton(
                                    text=self.current_ask[iter].
                                    __getattribute__(self.ask),
                                    akablase=self.current_ask[iter],
                                    on_press=self.handle_answer,
                                ))

        self.build_screen()
        if obj:
            self.popup.dismiss()

    def next_current_show(self):
        """
        Sets the next :class:`app.ext.AkaBlase` as :attr:`current_show`. Will only be
        executed, if :attr:`ask` has been set.
        """
        self.current_show = None

        if self.ask:
            set = False
            while not set:
                for a in self.akablasen:
                    if not a.used and (a.__getattribute__(self.show) is
                                       not None) and (a.__getattribute__(
                                           self.ask) is not None):
                        self.current_show = a
                        a.used = True
                        set = True
                        break

                # set everyone to not used, when everyone has been used.
                if not set:
                    for a in self.akablasen:
                        a.used = False

    def next_current_ask(self):
        """
        Sets the next :class:`app.ext.AkaBlase` as :attr:`current_ask`, if
        :attr:`current_show` is not None.
        """
        self.current_ask = []

        if self.current_show:
            self.current_ask.append(self.current_show)

            set = 0
            iterator = iter(self.akablasen)
            while set < 3:
                a = next(iterator)
                if (a.__getattribute__(self.ask) is not None):
                    if self.current_show.__getattribute__(
                            self.ask) != a.__getattribute__(self.ask) and (
                                self.current_show.__getattribute__(self.show)
                                != a.__getattribute__(self.show)):
                        self.current_ask.append(a)
                        set += 1

            shuffle(self.current_ask)

    def handle_answer(self, obj):
        """
        Handle an answer. Prompts a popup stating wether answer was correct or not. If
        not, includes the correct answer. Sets next values for :attr:`current_show` and
        :attr:`current_ask`. Rebuilds the screen.

        Args:
            obj (:class:`app.util.AkaButton` | :class:`app.ext.AkaImageButton`):
                The Button the anwser originated from.
        """

        # Local auxiliary function
        def show_solution(obj):
            popup_layout.clear_widgets()
            popup_layout.add_widget(
                AkaLabel(text='Richtig wäre:', size_hint_y=0.2))
            if self.ask == 'image':
                popup_layout.add_widget(
                    Image(
                        source=os.path.join(DATABASE, self.current_show.
                                            image)))
            else:
                popup_layout.add_widget(
                    AkaLabel(
                        text=self.current_show.__getattribute__(self.ask)))

            popup_layout.add_widget(
                AkaButton(
                    text='Nächster Versuch',
                    on_press=self.next,
                    size_hint_y=0.3,
                    pos_hint=VALIGN))

        correct = (obj.akablase.__getattribute__(
            self.ask) == self.current_show.__getattribute__(self.ask))

        if correct:
            self.next()
            return
        else:
            text = 'Leider falsch.'

        popup_layout = BoxLayout(
            # size_hint=(0.95, 0.95),
            spacing=10,
            # pos_hint={
            #     'center_y': 0.5,
            #     'center_x': 0.5
            # },
            orientation='vertical')

        popup = Popup(
            title='Das war ...',
            content=popup_layout,
            size_hint=(WIDTH, WIDTH * 1.5),
            auto_dismiss=False,
            separator_color=AkaBlas_Yellow[0],
            title_color=AkaBlas_Yellow[0])
        self.popup = popup

        popup_layout.add_widget(AkaLabel(text=text, on_press=popup.dismiss))

        button_layout = BoxLayout(spacing=10, size_hint_y=0.2)
        button_layout.add_widget(
            AkaButton(text='Nochmal\nversuchen', on_press=popup.dismiss))
        button_layout.add_widget(
            AkaButton(text='Lösung\nzeigen', on_press=show_solution))
        button_layout.add_widget(
            AkaButton(text='Nächster\nVersuch', on_press=self.next))
        popup_layout.add_widget(button_layout)

        popup.open()

    def build_screen(self):
        """
        Build the game screen. Call, if :attr:`show` or :attr:`ask` has changed or next
        question is needed.
        """
        self.clear_widgets()

        if self.show_widget:
            self.add_widget(self.show_widget)
        else:
            self.add_widget(self.show_dummy)

        if self.ask_widget:
            self.add_widget(self.ask_widget)
        else:
            self.add_widget(self.ask_dummy)

        self.add_widget(self.menu_button)

    def on_show(self, instance, value):
        """
        Callback when :attr:`show` has changed. Updates screen.

        Attributes:
            instance (:class:`Widget`): The Widget causing the change
            value (:obj:`str`): The new value of :attr:`show`
        """
        self.on_ask(None, self.ask)

    def on_ask(self, instance, value):
        """
        Callback when :attr:`ask` has changed. Updates screen.

        Attributes:
            instance (:class:`Widget`): The Widget causing the change
            value (:obj:`str`): The new value of :attr:`ask`
        """
        self.next()
