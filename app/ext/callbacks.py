#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Module containing callbacks that can be passed to bindings or on events functions.
"""

from kivy.app import App
from kivy.core.window import Window
from app.constants import SM


def exit(obj):
    """
    Call to exit the app.

    Args:
        obj: The object calling thin function
    """
    App.get_running_app().stop()
    Window.close()


def set_current_screen(obj):
    """
    Call to set the screen.

    Args:
        obj (:class:`app.util.AkaButton`): The button calling this function.
    """
    SM.current = obj.next_screen
