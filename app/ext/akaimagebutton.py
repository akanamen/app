#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the AkaImageButton class."""

from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image


class AkaImageButton(ButtonBehavior, Image):
    """
    A Image as Button.

    Attributes:
        next_screen (:obj:`str`): Optional. The name of the next screen, this button
            is supposed to call.
        akablase (:class:`app.ext.AkaBlase`): Optional. Can be used to pass information.

    Args:
        next_screen (:obj:`str`, optional): The name of the next screen, this button
            is supposed to call.
        akablase (:class:`app.ext.AkaBlase`, optional): Can be used to pass information.
        source (:obj:`str`): The path to the image.
    """

    def __init__(self, next_screen='', akablase=None, **kwargs):
        super(AkaImageButton, self).__init__(**kwargs)

        self.next_screen = next_screen
        self.akablase = akablase
