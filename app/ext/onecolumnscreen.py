#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the OneColumnScreen class."""

from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from .background import Background
from app.constants import VALIGN


class OneColumnScreen(Screen, Background):
    """
    Custom Screen. Widgets will be displayed horizontally centered in one coloumn, using
    BoxLayout.
    """

    def __init__(self, **kwargs):
        super(OneColumnScreen, self).__init__(**kwargs)

        self.layout = BoxLayout(
            size_hint=(0.95, 0.95),
            spacing=10,
            pos_hint={
                'center_y': 0.5,
                'center_x': 0.5
            },
            orientation='vertical')
        super(OneColumnScreen, self).add_widget(self.layout)

    def add_widget(self, widget):
        """
        Overwrite :meth:`add_widget` for use of BoxLayout. Widgets will be displayed
        horizontally centered in one coloumn.

        Args:
            widget (:class:`Widget`): The widget to add.
        """
        widget.pos_hint = VALIGN
        self.layout.add_widget(widget)

    def remove_widget(self, widget):
        """
        Overwrite :meth:`remove_widget` for use of BoxLayout.

        Args:
            widget (:class:`Widget`): The widget to remove.
        """
        self.layout.remove_widget(widget)

    def clear_widgets(self, **kwargs):
        """
        Overwrite :meth:`clear_widgets` for use of BoxLayout.
        """
        self.layout.clear_widgets(**kwargs)
