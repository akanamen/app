#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the Selection class"""

from app.util import AkaLabel
from app.constants import WIDTH, VALIGN
from kivy.uix.spinner import Spinner
from kivy.uix.boxlayout import BoxLayout


class Selection(object):
    """
    Widget for making a selection using a Spinner.

    Attributes:
        label (:class:`Label`): Description of what is to be selected.
        spinner (:class:`Spinner`): The Spinner.
        widget (:class:`Widget`): The widget containing both :attr:`label` and
            :attr:`spinner`.

    Args:
        label (:obj:`str`): Description of what is to be selected.
        values (Tuple(:obj:`str`)): Possible selections.
        default_text (:obj:`str`, optional): Text to display while no selection has been
            made.
    """

    def __init__(self, label, values, default_text='(Bitte wählen)', **kwargs):
        self.label = AkaLabel(text=label)
        self.spinner = Spinner(
            text=default_text,
            values=values,
            size_hint_x=WIDTH,
            pos_hint=VALIGN,
        )

        self.widget = BoxLayout(orientation='vertical', **kwargs)
        self.widget.add_widget(self.label)
        self.widget.add_widget(self.spinner)
