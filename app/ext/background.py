#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the Background class."""

from kivy.uix.behaviors import CoverBehavior
from kivy.uix.image import Image
from app.util import background


class Background(CoverBehavior, Image):
    """
    Custom Widget with a full screen AkaBlas_Gradient_Dark background
    """

    def __init__(self, **kwargs):
        super(Background, self).__init__(**kwargs)
        texture = background()
        self.reference_size = texture.size
        self.texture = background()
