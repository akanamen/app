#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the AboutScreen class."""

from kivy.uix.image import Image
from .callbacks import set_current_screen
from .onecolumnscreen import OneColumnScreen
from app.util import AkaButton, AkaLabel
from app.constants import REL_MEDIA_PATH as MEDIA
from app.constants import SM_MENU
from app import __version__
import os


class AboutScreen(OneColumnScreen):
    """
    Custom Screen. Use to display the about page.
    """

    def __init__(self, **kwargs):
        super(AboutScreen, self).__init__(**kwargs)

        self.add_widget(
            Image(
                source=os.path.join(MEDIA, 'AkaNamen_Logo_Yellow.png'),
                size_hint=(0.9, 0.9),
            ))

        description = ('Hilft Dir, die Namen, Spitznamen,\n'
                       'Instrumente etc. Deiner Mit-AkaBlasen\nzu lernen.')
        version = 'Version: {}\n'.format(__version__)
        self.add_widget(
            AkaLabel(
                text='AkaNamen App',
                valign='center',
                halign='center',
                size_hint_y=0.2))
        self.add_widget(
            AkaLabel(
                text=description,
                valign='center',
                halign='center',
                size_hint_y=0.2))
        self.add_widget(
            AkaLabel(
                text='Entwicklung:\nHinrich "Hirsch" Mahler',
                valign='center',
                halign='center',
                size_hint_y=0.2))
        self.add_widget(
            AkaLabel(
                text=version,
                valign='center',
                halign='center',
                size_hint_y=0.2))

        self.add_widget(
            AkaButton(
                text='Menü',
                size_hint_y=0.2,
                next_screen=SM_MENU,
                on_press=set_current_screen))
