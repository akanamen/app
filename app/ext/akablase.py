#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the AkaBlase class."""

from app.constants import REL_DATABASE_PATH as DATABASE
import json
import os


def get_akablasen():
    """
    Gives a list of all AkaBlasen.

    Returns:
        List[:class:`AkaBlase`]:
    """
    akablasen = []
    with open(os.path.join(DATABASE, 'akablasen'), 'r') as file:
        for line in file.readlines():
            akablasen.append(AkaBlase(line.strip('\n')))
    return akablasen


class AkaBlase(object):
    """
    Object representing a AkaBlase.

    Attributes:
        used (:obj:`bool`): Wether this AkaBlase has already been used as
            :attr:`app.ext.GameScreen.current_show`. Defaults to ``False`` on init.
        first_name (:obj:`str`): Optional. First name.
        last_name (:obj:`str`): Optional. Last name.
        nick_name (:obj:`str`): Optional. Nick name.
        instrument (:obj:`str`): Optional. Instrument.
        image (:obj:`str`): Optional. Name of Image file to be found at
            :attr:`util.constants.REL_DATABASE_PATH`.
        gender (:obj:`str`): Biological gender. Either ``male`` or ``female.

    Args:
        json_str (:obj:`str`): JSON representation of the AkaBlase.
    """

    ATTRIBUTES = [
        'first_name',
        'last_name',
        'nick_name',
        'instrument',
        'image',
        'gender',
    ]

    H_ATTRIBUTES = {
        'Vorname': 'first_name',
        'Nachname': 'last_name',
        'Spitzname': 'nick_name',
        'Instrument': 'instrument',
        'Bild': 'image',
    }
    """
    A dict where the keys are humen readible names for the attributes of
    :class:`app.ext.AkaBlase` and the values are the names of the attributes.
    """

    def __init__(self, json_str):
        self.used = False

        self.first_name = None
        self.last_name = None
        self.nick_name = None
        self.instrument = None
        self.image = None
        self.gender = None

        attr = json.loads(json_str)

        for a in AkaBlase.ATTRIBUTES:
            try:
                setattr(self, a, attr[a])
            except KeyError:
                setattr(self, a, None)

    @property
    def is_male(self):
        """
        Shortcut for::

            self.gender == 'male'
        """
        return self.gender == 'male'

    @property
    def is_female(self):
        """
        Shortcut for::

            self.gender == 'female'
        """
        return self.gender == 'female'
