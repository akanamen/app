#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the MenuScreen class."""

from kivy.uix.image import Image
from .selection import Selection
from .callbacks import exit, set_current_screen
from .onecolumnscreen import OneColumnScreen
from .akablase import AkaBlase
from app.util import AkaButton
from app.constants import REL_MEDIA_PATH as MEDIA
from app.constants import SM, SM_GAME, SM_ABOUT
import os


class MenuScreen(OneColumnScreen):
    """
    Custom Screen. Use to display the main menu.
    """

    def __init__(self, **kwargs):
        super(MenuScreen, self).__init__(**kwargs)

        self.options = AkaBlase.H_ATTRIBUTES.keys()
        self.show = None
        self.ask = None

        self.add_widget(
            Image(
                source=os.path.join(MEDIA, 'AkaNamen_Logo_Yellow.png'),
                size_hint=(0.9, 0.9),
            ))

        self.showSel = Selection(
            label='Zeige:', values=self.show_values, size_hint_y=0.4)
        self.showSel.spinner.bind(text=self.set_show)
        self.add_widget(self.showSel.widget)

        self.askSel = Selection(
            label='Frage:', values=self.ask_values, size_hint_y=0.4)
        self.askSel.spinner.bind(text=self.set_ask)
        self.add_widget(self.askSel.widget)

        self.add_widget(
            AkaButton(
                text='Spielen',
                size_hint_y=0.2,
                next_screen=SM_GAME,
                on_press=set_current_screen))
        self.add_widget(
            AkaButton(
                text='Info',
                size_hint_y=0.2,
                next_screen=SM_ABOUT,
                on_press=set_current_screen))
        self.add_widget(
            AkaButton(text='Beenden', size_hint_y=0.2, on_press=exit))

    @property
    def show_values(self):
        return tuple(x for x in self.options if x != self.ask)

    @property
    def ask_values(self):
        return tuple(x for x in self.options if x != self.show)

    def update_selection_values(self):
        """
        Makes sure the game doesn't ask for the attribute, it gives as clue.
        """
        self.showSel.spinner.values = self.show_values
        self.askSel.spinner.values = self.ask_values

    def set_show(self, spinner, value):
        """
        Update the value of :attr:`show`. Also update :attr:`app.ext.GameScreen.show`.

        Attributes:
            spinner (:class:`Spinner`): The spinner causing the update
            valeu (:obj:`str`): The new value.
        """
        self.show = value
        self.update_selection_values()
        SM.get_screen(SM_GAME).show = AkaBlase.H_ATTRIBUTES[self.show]

    def set_ask(self, spinner, value):
        """
        Update the value of :attr:`ask`. Also update :attr:`app.ext.GameScreen.ask`.

        Attributes:
            spinner (:class:`Spinner`): The spinner causing the update
            valeu (:obj:`str`): The new value.
        """
        self.ask = value
        self.update_selection_values()
        SM.get_screen(SM_GAME).ask = AkaBlase.H_ATTRIBUTES[self.ask]
