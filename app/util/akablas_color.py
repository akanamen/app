#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Colors as defined in the AkaBlas Corporate Design in RGBA Format. Each attribute is a list
containing all shades of the color.

Attributes:
    AkaBalas_BuleDark (:obj:`list`):
    AkaBlas_Yellow (:obj:`list`):
"""


def rgba_scale(rgba, add_88=False):
    """
    Args:
        rgba (:obj:`tuple`): RGBA tuple with values 0-255.
        add_88 (:obj:`bool`, otpional): If set to `True`, the formula will be
            ``(rgba+88)/255``. Needed e.g. for Spinner background_color.

    Returns:
        :obj:`tuple`: RGBA tuple with values 0-1.
    """
    if add_88:
        return tuple((x + 88) / 255. for x in rgba)
    else:
        return tuple(x / 255. for x in rgba)


AkaBlas_BlueDark = [(0, 26, 70, 255), (36, 74, 118, 255), (84, 105, 142, 255),
                    (134, 148, 178, 255), (190, 195, 213, 255)]

AkaBlas_Yellow = [(255, 236, 0, 255), (255, 239, 61, 255), (255, 243, 127,
                                                            255),
                  (255, 247, 177, 255), (255, 251, 218, 255)]
