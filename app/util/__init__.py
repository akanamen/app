"""
Utility classes and functions.
Just convenience stuff, does not really add functionality to kivy.

All constands and methds provided by the submodules are also directy available through
:class:`util`.
"""

from .akablas_color import (AkaBlas_BlueDark, AkaBlas_Yellow, rgba_scale)

from .akabutton import AkaButton
from .akalabel import AkaLabel
from .background import background

_all__ = [
    AkaBlas_BlueDark, AkaBlas_Yellow, AkaButton, AkaLabel, background,
    rgba_scale
]
