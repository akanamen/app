#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the AkaButton class."""

from kivy.uix.button import Button
from app.constants import WIDTH

# from app.constants import REL_MEDIA_PATH as MEDIA
# import os


class AkaButton(Button):
    """
    Custom Button.

    Attributes:
        next_screen (:obj:`str`): Optional. If set, can be used to indicate the next
            Screen to a ScreenManager. Defaults to ``''``.
        akablase (:class:`app.ext.AkaBlase`): Optional. Can be used to pass information.

    Args:
        next_screen (:obj:`str`, optional): If set, can be used to indicate the next
            Screen to a ScreenManager. Defaults to ``''``.
        akablase (:class:`app.ext.AkaBlase`, optional): Can be used to pass information.
        source (:obj:`str`): The path to the image.
    """

    def __init__(self, next_screen='', akablase=None, **kwargs):
        super(AkaButton, self).__init__(**kwargs)

        self.next_screen = next_screen
        self.akablase = akablase

        self.opacity = 1 if self.state == 'normal' else .3
        # self.background_normal = os.path.join(MEDIA, 'button_normal.png')
        # self.background_down = os.path.join(MEDIA, 'button_down.png')
        # self.border = [0, 0, 0, 0]

        # For some reason, calling rgba_scale(AkaBals_Yellow[0]) wont work here ...
        self.color = 1.0, 0.9254901960784314, 0.0, 1.0,
        self.bold = True
        self.font_size = 15

        self.size_hint_x = WIDTH
