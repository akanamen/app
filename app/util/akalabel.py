#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module contains the AkaLabel class."""

from kivy.uix.label import Label
from .akablas_color import AkaBlas_Yellow, rgba_scale


class AkaLabel(Label):
    """
    Custom Label.
    """

    def __init__(self, **kwargs):
        super(AkaLabel, self).__init__(**kwargs)

        self.color = rgba_scale(AkaBlas_Yellow[0])
        self.bold = True
        self.font_size = 15
