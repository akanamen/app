#!/usr/bin/env python
# -*- coding: utf-8 -*-

from kivy.graphics.texture import Texture
from .akablas_color import AkaBlas_BlueDark


def background():
    """
    Returns:
        Texture: The AkaBlas_Gradient_BlueDark as Texture
    """

    center_color = AkaBlas_BlueDark[2]
    border_color = AkaBlas_BlueDark[0]

    size = (100, 100)
    tex = Texture.create(size=size, colorfmt='rgba')

    sx_2 = size[0] / 2
    sy_2 = size[1] / 2

    buf = ''
    for x in range(-sx_2, sx_2):
        for y in range(-sy_2, sy_2):
            a = x / (1.0 * sx_2)
            b = y / (1.0 * sy_2)
            d = (a**2 + b**2)**.5

            for c in (0, 1, 2, 3):
                buf += chr(
                    max(
                        0,
                        min(
                            255,
                            int(center_color[c] * (1 - d)) + int(
                                border_color[c] * d))))

    tex.blit_buffer(buf, colorfmt='rgba', bufferfmt='ubyte')
    return tex
