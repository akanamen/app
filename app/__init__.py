#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Library for the AkaNamen app.

All constants provided by :class:`app.constants` are available directy through this
package.
"""

__version__ = '0.1'

from .constants import (REL_DATABASE_PATH, REL_MEDIA_PATH)

__all__ = ['REL_DATABASE_PATH', 'REL_MEDIA_PATH']
