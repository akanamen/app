#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Sots some global constants to simplify path dependencies and global variables.

Attributes:
    REL_DATABASE_PATH (:obj:`str`): The location of the AkaBlasen database relative to
        ``main.py``
    REL_MEDIA_PATH (:obj:`str`): The location of the media files relative to
        ``main.py``
    SM (:obj:`ScreenManager`): The ScreenManager managing the app.
    SM_MENU (:obj:`str`): The name of the menu screen.
    SM_GAME (:obj:`str`): The name of the game screen.
    SM_ABOUT (:obj:`str`): The name of the about screen.
    WIDTH (:obj:`float`): Use as size_hint_x so that everything will be equal in width.
    VALIGN (:obj:`dict`): Use as pos_hint for vertical alignement.
"""

from kivy.uix.screenmanager import ScreenManager, NoTransition

# Paths
REL_DATABASE_PATH = 'app/database'
REL_MEDIA_PATH = 'app/media'

# Size and Position
WIDTH = 0.66
VALIGN = {'center_x': 0.5}

# ScreenManager
SM = ScreenManager(transition=NoTransition())
SM_MENU = 'menu'
SM_GAME = 'game'
SM_ABOUT = 'about'
